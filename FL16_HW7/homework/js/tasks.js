// First
const getMaxEvenElement = (arr) => {
  const reduced = arr.reduce((acc, el) => {
    if (el % 2 === 0) {
      acc.push(el);
    }
    return acc;
  }, []);
  return Math.max(...reduced);
};

const arr = ["7", "3", "4", "2", "5"];

// Second
let a = 3;
let b = 5;

[a, b] = [b, a];

//Third
const getValue = (value) => value ?? "-";

// Fourth
const arrayOfArrays = [
  ["name", "dan"],
  ["age", "21"],
  ["city", "lviv"]
];

const getObjFromArray = (array) => {
  const object = {};
  for (let i of array) {
    object[i[0]] = i[1];
  }
  return object;
};

// Fifth
const obj1 = { name: "nick" };

const addUniqueId = (obj) => {
  let copy = Object.assign({}, obj);
  copy["id"] = Symbol();
  return copy;
};

// Sixth
const oldObj = {
  name: "willow",
  details: {
    id: 1,
    age: 47,
    university: "LNU"
  }
};

const getRegroupedObject = (obj) => {
  const {
    name: firstName,
    details: { id, age, university }
  } = obj;
  const user = { age, firstName, id };

  return { university, user };
};

// Seventh
const arr2 = [2, 3, 4, 2, 4, "a", "c", "a"];
const getArrayWithUniqueElements = (arr) =>
  arr.filter((element, index) => index === arr.indexOf(element));

// Eighth
const phoneNumber = "0123456789";

const hideNumber = (number) => {
  const hidenNumber = number.slice(number.length - 4);
  return hidenNumber.padStart(number.length, "*");
};

// Ninth
const add = (a = 3, b) => a && b ? a + b : new Error("b is required");

// Ten
function* generateItarebleSequence() {
  const arr = ["I", "love", "EPAM"];
  for (let i of arr) {
    yield i;
  }
}

const generatorObject = generateItarebleSequence();

for (let value of generatorObject) {
  console.log(value);
}
