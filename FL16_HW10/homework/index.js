class Magazine {
  constructor() {
    this.states = [
      new ReadyForPushNotification(),
      new ReadyForApprove(),
      new ReadyForPublish(),
      new PublishInProgress()
    ];

    this.current = this.states[0];
    this.staff = [];
    this.followers = [];
    this.articles = [];
  }
}

class ReadyForPushNotification{
  approve(name) {
    console.log(`Hello ${name}. You can't approve. We don't have enough 
        of publications`);
  }

  publish(name) {
    console.log(`Hello ${name}. You can't publish. We are creating 
    publications now.`);
  }
}

class ReadyForApprove {
  approve(name) {
    console.log(`Hello ${name} You've approved the changes`);
  }

  publish(name) {
    console.log(
      `Hello ${name} You can't publish. We don't have a manager's approval`
    );
  }
}

class ReadyForPublish {
  approve(name) {
    console.log(`Hello ${name} Publications have been already approved by you`);
  }

  publish(name) {
    console.log(`Hello ${name} You've recently published publications`);
  }
}

class PublishInProgress {
  approve(name) {
    console.log(
      `Hello ${name}. While we are publishing we can't do any actions`
    );
  }

  publish(name) {
    console.log(
      `Hello ${name}. While we are publishing we can't do any actions`
    );
  }
}

class MagazineEmployee {
  constructor(name, role, magazine) {
    this.name = name;
    this.role = role;
    this.magazine = magazine;
    magazine.staff.push({ name, role });
  }

  addArticle(article) {
    magazine.articles.push(article);
  }

  approve() {
    if (this.role !== "manager") {
      console.log("You do not have permissions to do it");
      return;
    }

    if (this.magazine.articles.length < 5) {
      this.magazine.current.approve(this.name);
      return;
    }

    let index = this.magazine.states.findIndex(
      (state) => state === this.magazine.current
    );

    this.magazine.current = this.magazine.states[++index];
    this.magazine.current.approve(this.name);
    this.magazine.current = this.magazine.states[++index];
  }

  publish() {
    this.magazine.current.publish(this.name);
  }
}

class Follower {
  constructor(follower) {
    this.follower = follower;
  }

  subscribeTo(magazine, topic) {
      magazine.followers.push({follower: this.follower, topic})
  }
}

const magazine = new Magazine();
const manager = new MagazineEmployee("Andrii", "manager", magazine);
const sport = new MagazineEmployee("Serhii", "sport", magazine);
const politics = new MagazineEmployee("Volodymyr", "politics", magazine);
const general = new MagazineEmployee("Olha", "general", magazine);

const iryna = new Follower('Iryna');
const maksym = new Follower('Maksym');
const mariya = new Follower('Mariya');

iryna.subscribeTo(magazine, 'sport');
maksym.subscribeTo(magazine, 'politics');
mariya.subscribeTo(magazine, 'politics');
mariya.subscribeTo(magazine, 'general');

sport.addArticle("something about sport");
politics.addArticle("something about politics");
general.addArticle("some general information");
politics.addArticle("something about politics again");
sport.approve(); //you do not have permissions to do it
manager.approve(); //Hello Andrii. You can't approve. We don't have enough of publications
politics.publish(); //Hello Volodymyr. You can't publish. We are creating publications now.
sport.addArticle("news about sport");
manager.approve(); //Hello Andrii. You've approved the changes
sport.publish(); //Hello Serhii. You've recently published publications.
