

const table = document.querySelector('.cell-game');
const specialCell = table.querySelector('.special');

function colorCells(e) {
  if (e.target !== specialCell && !e.target.classList.contains('first-col')) {
    e.target.classList.add('yellow');
  } else if (e.target.classList.contains('first-col')) {
    let targetCells = e.target.closest('tr').querySelectorAll('td');
    targetCells.forEach((cell) => {
      cell.classList.add('blue');
    });
  } else if (e.target === specialCell) {
    table.classList.add('green');
  }
}

table.addEventListener('click', colorCells);


const input = document.getElementById('input');
const label = document.querySelector('.label');
const button = document.querySelector('.button');
const success = document.querySelector('.success');

let regex = /\+380[0-9]{9}$/;
input.addEventListener('input', (e) => {
  if (!regex.test(e.target.value)) {
    label.style.display = 'block';
    input.style.borderColor = 'red';
  } else {
    label.style.display = 'none';
    button.disabled = false;
  }
});

button.addEventListener('click', () => {
  success.style.display = 'block';
});


const field = document.getElementById('container');
const ball = document.getElementById('ball');
const hook1 = document.querySelector('.hook1');
const hook2 = document.querySelector('.hook2');
const scoreTeam1 = document.querySelector('.scoreTeam1');
const scoreTeam2 = document.querySelector('.scoreTeam2');
const notification = document.querySelector('.notification');

field.addEventListener('click', (event) => {
  let fieldCoords = field.getBoundingClientRect();

  let ballCoords = {
    top:
      event.clientY - fieldCoords.top - field.clientTop - ball.clientHeight / 2,
    left:
      event.clientX -
      fieldCoords.left -
      field.clientLeft -
      ball.clientWidth / 2
  };

  if (ballCoords.top < 0) {
    ballCoords.top = 0;
  }

  if (ballCoords.left < 0) {
    ballCoords.left = 0;
  }

  if (ballCoords.left + ball.clientWidth > field.clientWidth) {
    ballCoords.left = field.clientWidth - ball.clientWidth;
  }

  if (ballCoords.top + ball.clientHeight > field.clientHeight) {
    ballCoords.top = field.clientHeight - ball.clientHeight;
  }

  ball.style.left = ballCoords.left + 'px';
  ball.style.top = ballCoords.top + 'px';
});

let num1 = 0;
let num2 = 0;

hook1.addEventListener('goalTeam1', (e) => {
  notification.textContent = e.detail.message;
  notification.style.color = 'red';
  scoreTeam2.textContent = `Team B: ${++num2}`;
});

hook1.addEventListener('click', () => {
  goalTeam1();
});

const goalTeam1 = () => {
  const event = new CustomEvent('goalTeam1', {
    detail: {
      message: 'Team B score!'
    }
  });
  hook1.dispatchEvent(event);
};

hook2.addEventListener('goalTeam2', (e) => {
  notification.textContent = e.detail.message;
  notification.style.color = 'blue';
  scoreTeam1.textContent = `Team A: ${++num1}`;
});

hook2.addEventListener('click', () => {
  goalTeam2();
});

const goalTeam2 = () => {
  const event = new CustomEvent('goalTeam2', {
    detail: {
      message: 'Team A score!'
    }
  });
  hook2.dispatchEvent(event);
};
