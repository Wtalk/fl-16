'use strict';


let hundred = 100;
let fifty = 50;
let twentyfive = 25;
let eight = 8;
let four = 4;
let two = 2;

let isStart = confirm('Do you want to play a game?');

if (!isStart) {
  alert('You did not become a billionaire, but can.');
} else {
  let range_coefficient = 0;
  let money_coefficient = 1;
  let user_money = 0;
  let game = true;

  while (game) {
    let continuePlay = 0;
    let max_number = eight + range_coefficient;
    let randomNumber = Math.floor(Math.random() * max_number);
    let attempts = 3;

    let prize_array = [hundred, fifty, twentyfive];

    for (let attempt = 0; attempt < attempts; attempt++) {
      let user_guess = +prompt(
        `Choose a roulette pocket number from 0 to ${max_number}\nAttempts left ${
          attempts - attempt
        }\nTotal prize: ${user_money}$\nPossible prize on current attempt: ${
          prize_array[attempt] * money_coefficient
        }$`
      );

      if (!isNaN(user_guess) && user_guess === randomNumber) {
        user_money += prize_array[attempt] * money_coefficient;
        continuePlay = confirm(
          `Congratulation, you won! Your price is: ${user_money}.Do you want to continue?`
        );
        break;
      }
    }
    if (continuePlay) {
      range_coefficient += four;
      money_coefficient *= two;
      continue;
    }
    alert(`Thank you foy your participation. Your price is: ${user_money}$`);
    let restart = confirm('You wanna play again?');
    user_money = 0;
    range_coefficient = 0;
    money_coefficient = 1;
    if (!restart) {
      break;
    }
  }
}
