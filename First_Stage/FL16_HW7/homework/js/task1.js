'use strict';

let initialAmount = parseFloat(prompt('Input initial amount:'));
let years = parseFloat(prompt('Input number of years:'));
let percentage = parseFloat(prompt('Input percentage of year:'));

console.log(initialAmount, years, percentage);

const thousand = 1000;
const hundred = 100;
const two = 2;

if (
  !isNaN(initialAmount) &&
  !isNaN(years) &&
  !isNaN(percentage) &&
  initialAmount >= thousand &&
  years >= 1 &&
  percentage <= hundred
) {
  let totalProfit = 0;
  let totalAmount = initialAmount;
  for (let year = 0; year < years; year++) {
    totalProfit += totalAmount * percentage / hundred;
    totalAmount = initialAmount + totalProfit;
  }
  alert(
    `Initial amount: ${initialAmount}\nNumber of years: ${parseInt(
      years
    )}\nPercentage of year: ${percentage}\n\nTotal profit: ${totalProfit.toFixed(
      two
    )}\nTotal amount: ${totalAmount.toFixed(two)}`
  );
} else {
  alert('Invalid input data');
}
