const isEquals = (a, b) => a === b;
const isBigger = (a, b) => a > b;
const storeNames = (...args) => [...args];
const getDifference = (a, b) => b > a ? b - a : a - b;

const negativeCount = (arr) => {
  let negativeValue = 0;
  for (const i of arr) {
    i < 0 ? negativeValue++ : null;
  }
  return negativeValue;
};

const letterCount = (string, letter) => {
  let occurences = 0;
  for (const i of [...string]) {
    i === letter ? occurences++ : null;
  }
  return occurences;
};

const countPoints = (collection) => {
  let scores = 0;
  for (let index = 0; index < collection.length; index++) {
    let splited = collection[index].split(":");
    for (let i = 0; i < splited.length; i++) {
      if (+splited[i] > +splited[i + 1]) {
        scores += 3;
      } else if (+splited[i] === +splited[i + 1]) {
        scores++;
      }
      break;
    }
  }
  return scores;
};
