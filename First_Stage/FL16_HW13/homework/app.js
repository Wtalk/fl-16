const appRoot = document.getElementById("app-root");

appRoot.insertAdjacentHTML(
  "beforeend",
  `<form>
<div>Please choose type of search:</div>
<div class="radios">
  <div>
    <input type="radio" id="byRegion" name="type" class="radio"/>
    <label for="byRegion">By Region</label>
  </div>
  <div>
    <input type="radio" id="byLanguage" name="type" class="radio"/>
    <label for="byLanguage">By Language</label>
  </div>
</div>
</form>
<div class="align-horizontal">
Please choose search query:
<select id="select" disabled class="selectComponent">
  <option selected hidden>Select value</option>
</select>
</div>
<table class="table"></table>`
);

const radios = document.querySelector(".radios");
const selectComponent = document.querySelector(".selectComponent");
const table = document.querySelector(".table");
let arrowArea;
let arrowCountry;


radios.addEventListener("change", () => {
  document.getElementById("select").disabled = false;
  table.textContent = "No items, please choose search query";

  selectComponent.innerHTML = "<option selected hidden>Select value</option>";
  if (document.querySelectorAll(".radio")[0].checked) {
    externalService.getRegionsList().forEach((el) => {
      selectComponent.innerHTML += `<option>${el}</option>`;
    });
  } else {
    externalService.getLanguagesList().forEach((el) => {
      selectComponent.innerHTML += `<option>${el}</option>`;
    });
  }
});

selectComponent.addEventListener("change", (e) => {
  const headerTable = [
    "Country <span class='arrowCountry'>&#8593;</span>",
    "Capital",
    "World Region",
    "Languages",
    "Area <span class='arrowArea'>&#8597;</span>",
    "Flag",
  ];

  
  table.innerHTML = "";
  let tr = document.createElement("tr");
  for (let i = 0; i < headerTable.length; i++) {
    let th = document.createElement("th");
    th.innerHTML = headerTable[i];
    tr.insertAdjacentElement("beforeend", th);
  }
  table.insertAdjacentElement("beforeend", tr);

  arrowCountry = document.querySelector(".arrowCountry");
  arrowArea = document.querySelector(".arrowArea");
  

  if (document.querySelectorAll(".radio")[0].checked) {
    createAndFillTable(externalService.getCountryListByRegion(e.target.value));
  } else {
    createAndFillTable(
      externalService.getCountryListByLanguage(e.target.value)
    );
  }

});


const createAndFillTable = (currentValue) => {
  const tableContent = [
    "name",
    "capital",
    "region",
    "languages",
    "area",
    "flagURL",
  ];
  for (let i = 0; i < tableContent.length; i++) {
    let tr = document.createElement("tr");
    for (let j = 0; j < tableContent.length; j++) {
      let td = document.createElement("td");

      if (j === 3) {
        for (let val of Object.values(currentValue[i][tableContent[j]])) {
          td.innerHTML += `${val} `;
          tr.insertAdjacentElement("beforeend", td);
        }
      } else if (j === 5) {
        td.innerHTML += `<img src="${currentValue[i][tableContent[j]]}"/>`;
        tr.insertAdjacentElement("beforeend", td);
      } else {
        td.innerHTML = currentValue[i][tableContent[j]];
        tr.insertAdjacentElement("beforeend", td);
      }
    }
    table.insertAdjacentElement("beforeend", tr);
  }
};


