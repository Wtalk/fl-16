const showBlock = document.querySelector(".form");
const confirmBtn = document.querySelector(".confirm");
const converterBtn = document.querySelector(".converter");

const onPageLoad = () => {
  if (prompt("Input event name", "meeting")) {
    showBlock.style.display = "block";
  }
};

confirmBtn.addEventListener("click", (event) => {
  const userName = document.getElementById("username").value;
  const userTime = document.getElementById("time").value;
  const userPlace = document.getElementById("place").value;

  event.preventDefault();
  const regex = /^(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$/;
  if (userName === "" || userTime === "" || userPlace === "") {
    alert("Input all data");
  } else {
    !regex.test(userTime)
      ? alert("Enter time in format hh:mm")
      : console.log(
          `${userName} has a meeting today at ${userTime} somewhere in ${userPlace}`
        );
  }
});

converterBtn.addEventListener("click", (event) => {
  event.preventDefault();

  const euro = +prompt("Input amount of euro");
  const dollar = +prompt("Input amount of dollar");
  const coefficientEuro = 33.52;
  const coefficientDollar = 27.76;

  if (euro > 0 && dollar > 0) {
    alert(
      `${euro} euros are equal ${(euro * coefficientEuro).toFixed(
        2
      )}hrns, ${dollar} are equal ${(dollar * coefficientDollar).toFixed(
        2
      )}hrns`
    );
  }
});
