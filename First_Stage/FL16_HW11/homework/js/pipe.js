function isFunction(functionToCheck) {
  return (
    functionToCheck && {}.toString.call(functionToCheck) === "[object Function]"
  );
}

const pipe = (value, ...funcs) => {
  return funcs.reduce((val, func, i) => {
    try {
      if (isFunction(func[i])) {
        throw new Error();
      }
      val = func(val);
    } catch (e) {
      return `Provided argument at position ${i} is not a function!`;
    }
    return val;
  }, value);
};

const replaceUnderscoreWithSpace = (value) => value.replace(/_/g, " ");

const capitalize = (value) =>
  value
    .split(" ")
    .map((val) => val.charAt(0).toUpperCase() + val.slice(1))
    .join(" ");
const appendGreeting = (value) => `Hello, ${value}!`;

const error = pipe("john_doe", replaceUnderscoreWithSpace, capitalize, "");

alert(error); 

const result = pipe(
  "john_doe",
  replaceUnderscoreWithSpace,
  capitalize,
  appendGreeting
);

alert(result); 
