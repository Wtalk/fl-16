function visitLink(path) {
  let currentValue = localStorage.getItem(path)
    ? parseInt(localStorage.getItem(path))
    : 0;
  let newValue = currentValue + 1;
  localStorage.setItem(path, newValue);
}

function viewResults() {
  let element = document.querySelector(".container");
  for (let i = 1; i <= 3; i++) {
    element.insertAdjacentHTML(
      "beforeend",
      `<li>you visited Page${i} ${localStorage.getItem(`Page${i}`)} time(s)</li`
    );
  }
}
