function reverseNumber(num) {
  let number = num.toString();
  let reserve = '';
  let start = number < 0 ? 1 : 0;
  for (let i = start; i < number.length; i++) {
    reserve = number[i] + reserve;
  }
  return +(number < 0 ? '-' + reserve : reserve);
}

function forEach(arr, func) {
  for (let element of arr) {
    func(element);
  }
}

function map(arr, func) {
  let result = [];
  forEach(arr, (el) => result.push(func(el)));
  return result;
}

function filter(arr, func) {
  let result = [];
  forEach(arr, (el) => {
    if (func(el)) {
      result.push(el);
    }
  });
  return result;
}

function getAdultAppleLovers(data) {
  let array = filter(data, (el) => el.age > 18 && el.favoriteFruit === 'apple');
  const name = map(array, (el) => el.name);
  return name;
}

function getKeys(obj) {
  let keys = [];
  for (const key in obj) {
    if(true) {
      keys.push(key);
    }
  }
  return keys;
}


function getValues(obj) {
  let values = [];
  for (const key in obj) {
    if(true) {
      values.push(obj[key]);
    }
  }
  return values;
}


function showFormattedDate(dateObj) {
  const monthNames = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec',
  ];
  let string = `It is ${dateObj.getDate()} of ${
    monthNames[dateObj.getMonth()]
  }, ${dateObj.getFullYear()}`;
  return string;
}
