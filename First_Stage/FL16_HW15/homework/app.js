const root = document.getElementById('root');

const getAge = (birthday) => {
  let ageDifMs = Date.now() - birthday.getTime();
  let ageDate = new Date(ageDifMs);
  return Math.abs(ageDate.getUTCFullYear() - 1970);
};

const getWeekDay = (dateobj) => {
  let options = { weekday: "long" };
  let result = new Intl.DateTimeFormat("en-US", options).format(dateobj);
  return result;
};

const getAmountDaysToNewYear = () => {
  let dateNow = new Date();
  let currentYear = dateNow.getFullYear();
  let newYearData = new Date(currentYear + 1, 0, 1);
  let millisecondsInDay = 86400000;
  let daysTillNewYear = Math.floor((newYearData - dateNow) / millisecondsInDay);
  return daysTillNewYear;
};

const getProgrammersDay = (year) => {
  if (getWeekDay(year) === true) {
    return "12 Sep, " + year + " (" + getWeekDay(new Date(year, 8, 12)) + ")";
  } else {
    return "13 Sep, " + year + " (" + getWeekDay(new Date(year, 8, 13)) + ")";
  }
}


function howFarIs(input) {
  let daysArr = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  let weekDay = input.charAt(0).toUpperCase() + input.slice(1);
  let today = new Date().getDay();
  let diff = daysArr.indexOf(weekDay) - today;
  if (diff > 0) {
    return `It's ${diff} day(s) left till ${weekDay}.`;
  } else if(diff === 0) {
      return `Hey, today is ${weekDay} =)`;
    } else {
        return `It's ${diff + 7} day(s) left till ${weekDay}.`;
      }
}


const isValidIdentifier = (string) => {
  const regex = /^[a-zA-Z_]*[_$]/;
  return regex.test(string);
};

const capitalize = (string) => {
  const regex = /(\b[a-z](?!\s))/g;
  string = string.replace(regex, (x) => x.toUpperCase());
  return string;
};

const isValidAudioFile = (file) => {
  const regex = /^[a-zA-Z]+.(mp3|flac|alac|aac)/;
  return regex.test(file);
};

const getHexadecimalColors = (string) => {
  const result = string.match(/#([a-f0-9]{3}){1,2}\b/gi);
  return result;
};

const isValidPassword = (password) => {
  const regex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
  return regex.test(password);
};

const addThousandsSeparators = (x) => {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

const getAllUrlsFromText = (url) => {
  return url.match(
    /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_.~#?&//=]*)/g
  );
};
