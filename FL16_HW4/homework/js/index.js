'use strict';

/**
 * Class
 * @constructor
 * @param size - size of pizza
 * @param type - type of pizza
 * @throws {PizzaException} - in case of improper use
 */
class Pizza {
    constructor(pizzaSize, pizzaType) {
        this.checkRightIngedients(pizzaSize,pizzaType);
        
        this.size = pizzaSize.size;
        this.type = pizzaType.type;
        this.extraIngredients = [];
        this.sizePrice = pizzaSize.price;
        this.typePrice = pizzaType.price;

    }

    checkRightIngedients(size, type) {
        if(arguments.length !== 2 || arguments[1] === undefined) {
            throw new PizzaException(`Required two arguments}`)
        }
        if (!Pizza.allowedSizes.has(size)) {
          throw new PizzaException('Invalid size');
        }
      
        if (!Pizza.allowedTypes.has(type) || size === type) {
          throw new PizzaException('Invalid type');
        }
      }

    addExtraIngredient(ingredient) {
        
        this.checkLength(arguments);
        this.checkIfExists(ingredient);

        if(this.extraIngredients.includes(ingredient)) {
            throw new PizzaException(`You can add one ingredient only once`);
        }

        this.extraIngredients.push(ingredient)
    }

    removeExtraIngredient(ingredient) {
        this.checkLength(arguments);
        this.checkIfExists(ingredient);

        if(!this.extraIngredients.includes(ingredient)) {
            throw new PizzaException(`There are no such ingredient in your order`);
        }

        const index = this.extraIngredients.indexOf(ingredient);
        this.extraIngredients.splice(index, 1);
    }

    getSize() {
        return this.size;
    }

    getPrice() {
        let totalPrice = this.typePrice + this.sizePrice;

        for(let ing of this.extraIngredients) {
            totalPrice += ing.price;
        }

        return totalPrice;
    }

    getExtraIngredients() {
        return this.extraIngredients;
    }

    getPizzaInfo() {
        let ingredients = '';
        for(let pizza of this.extraIngredients) {
            ingredients += pizza.ingredient + ' ';
        }
        console.log(`Size: ${this.size}, type: ${this.type}; extra ingredients: 
        ${ingredients}; price: ${this.getPrice()} UAH`);
    }

    checkLength(argv) {
        if(argv.length !== 1) {
            throw new PizzaException(`Required two arguments, given: ${arguments.length}`)
        }
    }

    checkIfExists(ingredient) {
        if(!Pizza.allowedExtraIngredients.has(ingredient)) {
            throw new PizzaException(`There is no such ingredient - ${ingredient} in allowedExtraIngredients`);
        }
    }
}

/* Sizes, types and extra ingredients */
Pizza.SIZE_S = {
    size: 'S',
    price: 50
}
Pizza.SIZE_M = {
    size: 'M',
    price: 75
}
Pizza.SIZE_L = {
    size: 'L',
    price: 100
}

Pizza.TYPE_VEGGIE = {
    type: 'VEGGIE',
    price: 50
}
Pizza.TYPE_MARGHERITA = {
    type: 'MARGHERITA',
    price: 60
}
Pizza.TYPE_PEPPERONI = {
    type: 'PEPPERONI',
    price: 70
}

Pizza.EXTRA_TOMATOES = {
    ingredient: 'CHEESE',
    price: 5
}
Pizza.EXTRA_CHEESE = {
    ingredient: 'CHEESE',
    price: 7
}
Pizza.EXTRA_MEAT = {
    ingredient: 'MEAT',
    price: 9
}

/* Allowed properties */
Pizza.allowedSizes = new Set([Pizza.SIZE_S, Pizza.SIZE_M,Pizza.SIZE_L])
Pizza.allowedTypes = new Set([Pizza.TYPE_VEGGIE, Pizza.TYPE_MARGHERITA, Pizza.TYPE_PEPPERONI]);
Pizza.allowedExtraIngredients = new Set([Pizza.EXTRA_TOMATOES,Pizza.EXTRA_CHEESE,Pizza.EXTRA_MEAT]);


/**
 * Provides information about an error while working with a pizza.
 * details are stored in the log property.
 * @constructor
 */
class PizzaException { 
    constructor(message) {
        this.message = message;
    }

 }

/* It should work */ 
// small pizza, type: veggie
//  let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// // // add extra meat
//  pizza.addExtraIngredient(Pizza.EXTRA_MEAT);
// // // check price
//  console.log(`Price: ${pizza.getPrice()} UAH`); //=> Price: 109 UAH
// // // add extra corn
// pizza.addExtraIngredient(Pizza.EXTRA_CHEESE);
// // // add extra corn
//  pizza.addExtraIngredient(Pizza.EXTRA_TOMATOES);
// // // check price
//  console.log(`Price with extra ingredients: ${pizza.getPrice()} UAH`); // Price: 121 UAH
// // // check pizza size
//  console.log(`Is pizza large: ${pizza.getSize() === Pizza.SIZE_L}`); //=> Is pizza large: false
// // // remove extra ingredient
// pizza.removeExtraIngredient(Pizza.EXTRA_CHEESE);
// console.log(`Extra ingredients: ${pizza.getExtraIngredients().length}`); //=> Extra ingredients: 2
// console.log(pizza.getPizzaInfo()); //=> Size: SMALL, type: VEGGIE; extra ingredients: MEAT,TOMATOES; price: 114UAH.

// examples of errors
//let pizz = new Pizza(Pizza.SIZE_S); // => Required two arguments, given: 1

//let piz = new Pizza(Pizza.SIZE_S, Pizza.SIZE_S); // => Invalid type

// let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT);
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT); // => Duplicate ingredient

// let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
//  pizza.addExtraIngredient(Pizza.EXTRA_MEAT); // => Invalid ingredient
