const bodyHtml = document.getElementsByTagName("body");
const contentBox = document.getElementById("content");
const getUsersBtn = document.getElementById("getUsersBtn");

getUsersBtn.addEventListener("click", function () {
  getUsers(`https://jsonplaceholder.typicode.com/users`);

  contentBox.addEventListener("click", function (e) {
    const userID = e.target.id.split("").pop();
    const user = document.getElementById(`user${userID}`);
    if (e.target.classList.contains("btnDelete")) {
      user.remove();
      fetch(`https://jsonplaceholder.typicode.com/users/${userID}`, {
        method: "DELETE",
        headers: {
          "Content-type": "application/json; charset=UTF-8"
        }
      })
        .then((response) => response.json())
    }

    if (e.target.classList.contains("btnUpdate")) {
      let name = prompt("Change the name");
      let userName = prompt("Change the user name");
      let email = prompt("Change email");
      const userInfo = document.getElementById(`userInfo${userID}`);
      userInfo.innerHTML = `Name : ${name}; ID : ${userID}; User Name : ${userName}; Email :  ${email}`;

      fetch(`https://jsonplaceholder.typicode.com/users/${userID}`, {
        method: "PUT",
        body: JSON.stringify({
          name,
          userName,
          email
        }),
        headers: {
          "Content-type": "application/json; charset=UTF-8"
        }
      })
        .then((response) => response.json())

    }
  });
});

const getUsers = (url) => {
  fetch(url)
    .then((response) => response.json())
    .then((json) => {
      contentBox.innerHTML = "";

      for (let i = 0; i < json.length; i++) {
        const div = document.createElement("div");
        div.setAttribute("id", `user${json[i].id}`);

        const paragraph = document.createElement("p");
        paragraph.setAttribute("id", `userInfo${json[i].id}`);
        paragraph.innerHTML = `Name : ${json[i].name}; 
        ID : ${json[i].id}; User Name : ${json[i].username}; Email :  ${json[i].email}`;

        const btnDelete = document.createElement("button");
        btnDelete.setAttribute("id", `btnDelete${json[i].id}`);
        btnDelete.classList.add(`btnDelete`);
        btnDelete.innerHTML = "Delete";

        const btnUpdate = document.createElement("button");
        btnUpdate.setAttribute("id", `btnUpdate${json[i].id}`);
        btnUpdate.classList.add(`btnUpdate`);
        btnUpdate.innerHTML = "Update";

        div.insertAdjacentElement("beforeend", paragraph);
        div.insertAdjacentElement("beforeend", btnDelete);
        div.insertAdjacentElement("beforeend", btnUpdate);
        contentBox.insertAdjacentElement("beforeend", div);
      }
    });
}
