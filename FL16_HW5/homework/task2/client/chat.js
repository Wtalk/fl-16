const statuss = document.getElementById("status");
const messages = document.getElementById("messages");
const form = document.getElementById("form");
const input = document.getElementById("input");

const ws = new WebSocket("ws://localhost:8080");

function setStatus(value) {
  statuss.innerHTML = value;
}

function printMessage(data) {
  const li = document.createElement("li");

  const parsedData = JSON.parse(data);

  li.innerHTML = `<i>${parsedData.userName}</i>: 
  <i style="font-size:19px">${parsedData.text}</i><br/><b style="font-size:10px">at ${parsedData.time}</b>`;
  messages.appendChild(li);
}

const userName = prompt("What is your name?");

form.addEventListener("submit", (event) => {
  event.preventDefault();

  let today = new Date();
  let time =
    today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
  const data = JSON.stringify({
    userName,
    time,
    text: input.value
  });

  ws.send(data);
  input.value = "";
});

ws.onopen = () => setStatus("ONLINE");

ws.onclose = () => setStatus("DISCONNECTED");

ws.onmessage = (response) => {
  printMessage(response.data);
};
